﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoSingleton<GameManager> 
{
    [HideInInspector]
    public MainMenuController mainMenuController;
    [HideInInspector]
    public InGameController inGameController;
    [HideInInspector]
    public GameOverController gameOverController;

    [HideInInspector]
    public TimeController timeController;
    [HideInInspector]
    public InputController inputController;
    [HideInInspector]
    public PlayerController playerController;

    [HideInInspector]
    public PoolManager poolManager;

    public GameState _currentState;
    public GameState currentState 
    { 
        get
        {
            return _currentState;
        }
    }


    private GameState _previousState;

    void Awake()
    {
        poolManager = FindObjectOfType<PoolManager>();

        playerController = FindObjectOfType<PlayerController>();
        timeController = FindObjectOfType<TimeController>();
        inputController = FindObjectOfType<InputController>();
            
        mainMenuController = FindObjectOfType<MainMenuController>();
        inGameController = FindObjectOfType<InGameController>();
        gameOverController = FindObjectOfType<GameOverController>();


    }

    void Start()
	{
        
	}

	public void SetGameState(GameState state)
	{
        GameState prevState = _currentState;
		_currentState = state;
        _previousState = prevState;

		OnStartState ();

        Debug.LogError("prev state " + _previousState.ToString()
                               + " current state " + _currentState.ToString());
	}

	void OnStartState()
	{
		switch(_currentState)
		{
    		case GameState.MainMenu:
    			Time.timeScale = 1;
    			break;

		    case GameState.InGame:
			        Time.timeScale = 1;

				    if (_previousState == GameState.GameOver) {

				    }
    		break;
    		case GameState.GameOver:
    			Time.timeScale = 1;
    			break;
		}
	}

	void OnUpdateState()
	{
		switch(_currentState)
		{
    		case GameState.MainMenu:
    			break;
			case GameState.InGame:
    			break;
    		case GameState.GameOver:
    			break;
		}
	}

    void OnFixedUpdateState()
    {
        switch (_currentState)
        {
            case GameState.MainMenu:
                break;
            case GameState.InGame:
                break;
            case GameState.GameOver:
                break;
        }
    }

	void Update () 
	{
		OnUpdateState ();
    }

    void FixedUpdate()
    {
        
    }
}

public enum GameState
{
	MainMenu,
	InGame,
	GameOver,
}