﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

public class UIManager : MonoSingleton<UIManager> 
{
	public UIData[] uiData;

	private Dictionary<GameState , GameObject> uiDictionary = new Dictionary<GameState, GameObject>();

	void Awake()
	{
		Application.targetFrameRate = 60;

		for (int i = 0; i < uiData.Length; i++) 
		{
			uiDictionary.Add( uiData[i].type, uiData[i].panel );
		}
	}

	void Start()
	{
		Show(GameState.MainMenu);
	}

	public T GetContoller<T> (GameState type) where T : Component
	{
		if (uiDictionary.ContainsKey (type))
			return uiDictionary [type].GetComponentInParent<T> ();

		return null;
	}

	public void Show(GameState type, float time)
	{
		if (uiDictionary.ContainsKey (type))
			uiDictionary [type].GetComponentInParent<BasicController> ().OnEnter ();
	}

	public void Hide(GameState type, float time)
	{
		if (uiDictionary.ContainsKey (type))
        {
            uiDictionary[type].GetComponentInParent<BasicController>().OnExit();
        }
	}

    public void Show(GameState type)
    {
        if (uiDictionary.ContainsKey(type))
        {
            BasicController bc = uiDictionary[type].GetComponentInParent<BasicController>();

			if (type == GameState.MainMenu) {
				MainMenuController mc = bc as MainMenuController;
				mc.OnEnter ();
			} else if (type == GameState.InGame) {
				InGameController ic = bc as InGameController;
				ic.OnEnter ();
			}else if (type == GameState.GameOver) {
				GameOverController goc = bc as GameOverController;
				goc.OnEnter ();
			} 
        }
    }

    public void Hide(GameState type)
    {
        BasicController bc = uiDictionary[type].GetComponentInParent<BasicController>();

		if (type == GameState.MainMenu) {
			MainMenuController mc = bc as MainMenuController;
			mc.OnExit ();
		} else if (type == GameState.InGame) {
			InGameController ic = bc as InGameController;
			ic.OnExit ();
		} else if (type == GameState.GameOver) {
			GameOverController goc = bc as GameOverController;
			goc.OnExit ();
		} 
    }

}

[System.Serializable]
public class UIData
{
	public GameState type;
	public GameObject panel;
}


