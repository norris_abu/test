﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class InputManager : MonoSingleton <InputManager>
{
    public Action OnClick;

    void Update()
    {
        OnClickDelegate();
    }

    void OnClickDelegate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (OnClick != null)
                OnClick();
        }
    }
}
