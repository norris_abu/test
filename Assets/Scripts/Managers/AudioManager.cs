using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager {
	
	private static AudioManager instance = null;
	private GameObject instance_gameObject = null;
	private float bgm_volume = Application.isEditor ? 0.35f : 0.35f;
	private float sfx_volume = Application.isEditor ? 0.7f : 0.7f;
	
	private List<AudioSource> sfx_audiosouces = new List<AudioSource>();
	private int sfx_base_count = 10;
	private string bgm_playing = string.Empty;
	private bool bgm_playing_loop = false;
	private Dictionary<string,AudioClip> clip_bank = new Dictionary<string, AudioClip>();
	private Dictionary<AudioSource,Transform> asrouce_transform = new Dictionary<AudioSource, Transform>();

	private bool bgmOn = true;
	private bool sfxOn = true;
	
	public static AudioManager Instance
	{
		get {
			if (instance == null)
			{
				instance = new AudioManager();
			}
			return instance;
		}
	}
	
	public GameObject g_instance_parent
	{
		get{
		return instance_gameObject;
		}
	}

	private AudioSource bgm_ausource = null;
	public AudioSource get_bgm_ausource {
		get {
			if ((bgm_ausource = instance_gameObject.GetComponent<AudioSource>()) == null) {
				bgm_ausource = instance_gameObject.AddComponent<AudioSource>();
			}
			return bgm_ausource;
		}
	}
	
	private AudioManager()
	{
		instance_gameObject = new GameObject("AudioManager");
		GameObject.DontDestroyOnLoad(instance_gameObject);	
		instance_gameObject.AddComponent<AudioListener>();
			
		for (int x = 0 ; x < sfx_base_count; x++)
		{
			GameObject g = new GameObject("sfx"+(x+1));
			AudioSource asrc = g.AddComponent<AudioSource>();
			GameObject.DontDestroyOnLoad(g);
			g.transform.parent = instance_gameObject.transform;
			g.transform.localPosition = Vector3.zero;
			sfx_audiosouces.Add(asrc);
			asrouce_transform.Add(asrc,g.transform);
		}
	}
	
	
	public Transform get_transform_from_asrouce(AudioSource asrc )
	{
		return asrouce_transform[asrc];
	}
	
	public AudioSource get_source_inbank()
	{
		AudioSource ret = null;
		foreach (AudioSource g in sfx_audiosouces)
		{
			if (!g.isPlaying || !g.gameObject.activeSelf)
				ret = g;
		}
		if (ret == null)
		{
			GameObject g = new GameObject("sfx"+(sfx_audiosouces.Count));
			AudioSource asrc = g.AddComponent<AudioSource>();
			GameObject.DontDestroyOnLoad(g);
			g.transform.parent = instance_gameObject.transform;
			g.transform.localPosition = Vector3.zero;
			sfx_audiosouces.Add(asrc);
			asrouce_transform.Add(asrc,g.transform);
			ret = asrc;
		}
		return ret;
			
	}

	public void play_sfx(AudioClip aclp) {
		if (aclp == null)
		{

			return;
		}
		
		AudioSource asrc = get_source_inbank();
		Transform tt = get_transform_from_asrouce(asrc);

		tt.parent = instance_gameObject.transform;
		tt.localPosition = Vector3.zero;

		asrc.clip = aclp;
		asrc.volume = sfx_volume;
		asrc.Play();
	}

	public void play_sfx(string path, Vector3 p, float sb = 0) { play_sfx (path, p, 1, true, 1, sb); }
	public void play_sfx(string path, Vector3 p , float rvol, bool connected_listner, float pitch, float sb = 0)
	{
		if (!sfxOn)
			return;

		AudioClip aclp = null;
		if (clip_bank.ContainsKey(path))
			aclp = clip_bank[path];
		else
		{
		 	aclp = Resources.Load(path) as AudioClip;
			if (aclp != null)
				clip_bank.Add(path,aclp);
		}
		
		if (aclp == null)
		{
			Debug.LogError (path);
			return;
		}
		
		AudioSource asrc = get_source_inbank();
		Transform tt = get_transform_from_asrouce(asrc);
		if (connected_listner)
		{
			tt.parent = instance_gameObject.transform;
			tt.localPosition = Vector3.zero;
		}
		else
		{
			if (tt.parent != null)
				tt.parent = null;
			tt.position = p;
			
		}
		asrc.pitch = pitch;
		asrc.clip = aclp;
		asrc.spatialBlend = sb;
		asrc.volume = sfx_volume * rvol;
		asrc.Play();
		
	}
	
	
	public void play_bgm(string path, bool isLoop = true) 
	{

		if (!bgmOn)
			return;
		if (path == bgm_playing)
        {
            get_bgm_ausource.Play();
        }
		AudioClip aclp = Resources.Load(path) as AudioClip;
		if (aclp == null) {
			Debug.LogError(" NOT EXISITING BGM ");
			Debug.LogError (path);			
			return;
		}

		get_bgm_ausource.clip = aclp;
		get_bgm_ausource.loop = isLoop;
		get_bgm_ausource.volume = bgm_volume;
		get_bgm_ausource.Play();
		bgm_playing = path;
	}

	public void stop_bgm() {
		get_bgm_ausource.Stop ();
		bgm_playing = null;
	}

	public bool isBGMOn() {
		return bgmOn;
	}

	public bool isSFXOn() {
		return sfxOn;
	}

	public bool toggleBGM() {
		bgmOn = !bgmOn;
		if (isBGMOn())
			play_bgm (bgm_playing, bgm_playing_loop);
		else
			stop_bgm ();
		return bgmOn;
	}
	
	public bool toggleSFX() {
		sfxOn = !sfxOn;
		return sfxOn;
	}

	public void bgmon(bool ison){
		bgmOn = ison;
	}

	public void sfxon(bool ison){
		sfxOn = ison;
	}

}
