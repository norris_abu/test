﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    public float tempo;
   

    public float barInterval;

    public float yellowPercentage;
    public float redPercentage;

    private float defaultyellowPercentage = 20f;
    private float defaultredPercentage = 20f;
    private float defaultTempo = 60f
        ;

    float barTime = 0f;
    float noteTime = 0f;
    float speedTempo = 1;

    float bgmLength = 0;
    float bgmTime = 0;

    bool isReadyForNextLevel = false;

    public float SpeedTempo { get { return speedTempo; } }

    InGameController ingameController; 

    // Start is called before the first frame update
    void Start()
    {
        ingameController = GameManager.Instance.inGameController;
    }

    // Update is called once per frame
    void Update()
    {

        if (GameManager.Instance.currentState != GameState.InGame)
            return;

        if (AudioManager.Instance.get_bgm_ausource != null)
        {
            AudioSource bgmSource = AudioManager.Instance.get_bgm_ausource;
            bgmTime = bgmSource.time;
            bgmLength = bgmSource.clip.length;
            if(bgmTime > bgmLength - 0.2f)
            {
                if(!isReadyForNextLevel)
                {
                    ingameController.Reset();

                    GameManager.Instance.poolManager.ResetObjectList("YellowNote");
                    GameManager.Instance.poolManager.ResetObjectList("RedNote");
                    GameManager.Instance.poolManager.ResetObjectList("Bar");
                    GameManager.Instance.playerController.AddLevel();

                    tempo += 5;
                    
                    yellowPercentage += Random.Range(5, 10);
                    redPercentage += Random.Range(5, 10);

                    yellowPercentage = Mathf.Clamp(yellowPercentage, 0, 90);
                    redPercentage = Mathf.Clamp(redPercentage, 0, 90);

                    ingameController.ShowLevel(GameManager.Instance.playerController.Level);

                    isReadyForNextLevel = true;
                }  
            }else
                isReadyForNextLevel = false;
        }

        if (bgmTime > bgmLength - 10)
        {
            barTime = 0;
            noteTime = 0;
            return;
        }
            
        speedTempo = tempo / defaultTempo;

        barTime += Time.deltaTime * speedTempo;
        noteTime += Time.deltaTime * speedTempo;

        if (AlmostEqual(barTime, barInterval * 4f))
        {
            GameObject bar = GameManager.Instance.poolManager.GetObject("Bar");
            bar.transform.position = new Vector3(0, 10, 0f);

            Move move = bar.GetComponent<Move>();
            move.speed = move.defaultSpeed * (tempo / defaultTempo);

            barTime = 0f;
        }

        if (noteTime >= barInterval)
        {
            noteTime = 0f;
        }

        if (AlmostEqual( noteTime, 0f) || 
            AlmostEqual( noteTime, barInterval * 0.25f ) || 
            AlmostEqual( noteTime, barInterval * 0.5f ) || 
            AlmostEqual( noteTime, barInterval * 0.75f ) ||
            AlmostEqual( noteTime, barInterval))
        {
            int yellowRand = Random.Range(0,100);
            int redRand = Random.Range(0, 100);
            if (yellowRand < yellowPercentage)
            {
                GameObject yellowNote = GameManager.Instance.poolManager.GetObject("YellowNote");
                yellowNote.transform.position = new Vector3(-2, 10, 0f);

                Note yNote = yellowNote.GetComponent<Note>();
                Move move = yellowNote.GetComponent<Move>();
                move.speed = move.defaultSpeed * (tempo / defaultTempo);

                ingameController.AddNotes(yNote);
            }
            if (redRand < redPercentage)
            {
                GameObject redNote = GameManager.Instance.poolManager.GetObject("RedNote");
                redNote.transform.position = new Vector3(2, 10, 0f);

                Note rNote = redNote.GetComponent<Note>();
                Move move = redNote.GetComponent<Move>();
                move.speed = move.defaultSpeed * (tempo / defaultTempo);

                ingameController.AddNotes(rNote);
            }
        }
    }

    public void Reset()
    {
        tempo = defaultTempo;

        yellowPercentage = defaultyellowPercentage;
        redPercentage = defaultredPercentage;

        barTime = 0;
        noteTime = 0;

        GameManager.Instance.poolManager.ResetObjectList("YellowNote");
        GameManager.Instance.poolManager.ResetObjectList("RedNote");
        GameManager.Instance.poolManager.ResetObjectList("Bar");
    }

    // Return true if the two values are almost equal, according to the given tolerance.
    public static bool AlmostEqual(float a, float b, float eps = 0.01f)
    {
        return (a >= b - eps && a <= b + eps);
    }
}
