﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConstants
{
    // BGM
    public static string BGM_FROZY = "Sounds/Frozy";
    public static string BGM_SYSTEMSHOCK = "Sounds/System Shock";
    public static string BGM_TWILIGHT = "Sounds/Twilight";

    // SFX
    public static string SFX_HIT_YELLOWNOTE = "Sounds/HitYellowNote";
    public static string SFX_HIT_REDNOTE = "Sounds/HitRedNote";
    public static string SFX_MENU_SELECT = "Sounds/MenuSelect";
    public static string SFX_TAPHALO_DEFAULT = "Sounds/TapNote";
}
