﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIHelper : MonoSingleton<UIHelper> 
{
    private const float referenceDPI = 96.0f;        // Typical desktop screen DPI.
    private const float referenceUIScale = 1f;    // UI scale factor at the reference DPI.
    private const float minScreenPixels = 670;

    private static float lastScaleFactor = 0.0f;
    private static float lastScreenHeight = 0.0f;

    public static CanvasScaler canvasScaler;

    public static float dpi
    {
        get
        {
            float _dpi = Screen.dpi;
            float _trueDPI = 1f;
            float _referenceDPI = 1f;

            if (_dpi <= 1.0f)
                return referenceDPI;

            if (_dpi <= referenceDPI)
            {
                _trueDPI = 1;
                _referenceDPI = 1;
            }

            return _dpi * _trueDPI / _referenceDPI;
        }
    }

    public static T FindObject<T>(Transform parent, string name) where T : Component
    {
        List<T> list = GetObject<T>(parent);
        return list.Find(x => x.name == name);
    }

    public static List<T> GetObject<T>(Transform parent) where T : Component
    {
        return parent.GetComponentsInChildren<T>(true).ToList();
    }

    public static bool IsOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    public static void UpdateUIScale()
    {
        float scale = referenceUIScale * dpi / referenceDPI;

        // Shrink for small devices.
        int w = Mathf.Min(Screen.width, Screen.height);
        if (scale > w / minScreenPixels)
            scale = w / minScreenPixels;

        if (scale != lastScaleFactor)
        {
            lastScaleFactor = scale;
            canvasScaler.scaleFactor = scale;
        }
    }
}
