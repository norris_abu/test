﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class GameOverView : BasicView 
{
    const string BUTTON_PLAY = "Play Again";
    const string BUTTON_HOME = "Home";
    const string BUTTON_SHARE = "Share";
    const string BUTTON_WATCH = "Sound";

    const string TXT_SCORE = "TxtScore";

    [HideInInspector]
    public Button btnPlay;
    [HideInInspector]
    public Button btnHome;
    [HideInInspector]
    public Button btnShare;
    [HideInInspector]
    public Button btnSound;

    private Text txtSound;

    [HideInInspector]
    public TextMeshProUGUI txtScore;

    public void Init()
    {
        btnPlay = UIHelper.FindObject<Button>(transform, BUTTON_PLAY);
        btnHome = UIHelper.FindObject<Button>(transform, BUTTON_HOME);
        btnShare = UIHelper.FindObject<Button>(transform, BUTTON_SHARE);
        btnSound = UIHelper.FindObject<Button>(transform, BUTTON_WATCH);

        txtScore = UIHelper.FindObject<TextMeshProUGUI>(transform, TXT_SCORE);
        txtSound = btnSound.GetComponentInChildren<Text>();
    }

    public void SetButtonSoundText(bool isOn)
    {
        txtSound.text = isOn ? "SOUND ON" : "SOUND OFF";
    }

    public void SetScore(int score)
    {
        txtScore.text = string.Format("{0}", score);
    }
}