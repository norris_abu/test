﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class BasicView : MonoBehaviour 
{
	protected CanvasGroup canvasGroup;

	void Awake()
	{
		canvasGroup = GetComponent<CanvasGroup> ();
	}

	public virtual void ShowMenu(float time = 1, Action callback = null)
	{
        canvasGroup.DOFade (1f, time)
			.OnComplete( () => 
				{
					canvasGroup.blocksRaycasts = true;
					canvasGroup.interactable = true;

					if(callback != null)
						callback();
				} ).SetUpdate(true);
	}

	public virtual void HideMenu(float time = 1, Action callback = null)
	{
        canvasGroup.DOFade (0f, time)
			.OnComplete( () => 
				{
					canvasGroup.blocksRaycasts = false;
					canvasGroup.interactable = false;

					if(callback != null)
						callback();
				} ).SetUpdate(true);
	}

    public virtual void Reset()
    {
        
    }

    public virtual void Show(Action callback = null)
    {
        canvasGroup.DOKill();
        canvasGroup.interactable = true;
        canvasGroup.DOFade(1f, 0.2f)
            .OnComplete(() =>
            {
                canvasGroup.blocksRaycasts = true;

                if (callback != null)
                    callback();
            }).SetUpdate(true);
    }

    public virtual void Hide(Action callback = null)
    {
        canvasGroup.DOKill();
        canvasGroup.interactable = false;
        canvasGroup.DOFade(0f, 0.2f)
            .OnComplete(() =>
            {
                canvasGroup.blocksRaycasts = false;

                if (callback != null)
                    callback();
            }).SetUpdate(true);
    }
}
