﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using TMPro;

public class InGameView : BasicView
{
    const string BUTTON_PAUSE = "Pause";

    const string RECT_COMBO = "ComboPanel";
    const string TXT_COMBO = "TxtCombo";
    const string TXT_MISS = "TxtMiss";
    const string TXT_GOOD = "TxtGood";
    const string TXT_LEVEL = "TxtLevel";

    [HideInInspector]
    public Button btnPause;
    [HideInInspector]
    public TextMeshProUGUI txtCombo;
    [HideInInspector]
    public TextMeshProUGUI txtMiss;
    [HideInInspector]
    public TextMeshProUGUI txtGood;
    [HideInInspector]
    public TextMeshProUGUI txtLevel;
    [HideInInspector]
    public RectTransform rectComboPanel;

    [HideInInspector]
    public RectTransform rectMiss;
    [HideInInspector]
    public RectTransform rectGood;

    float defaultGoodPos = 0f;
    float defaultMissPos = 0f;

    public void Init()
    {
        btnPause = UIHelper.FindObject<Button>(transform, BUTTON_PAUSE);

        rectComboPanel = UIHelper.FindObject<RectTransform>(transform, RECT_COMBO);
        txtCombo = UIHelper.FindObject<TextMeshProUGUI>(transform, TXT_COMBO);
        txtMiss = UIHelper.FindObject<TextMeshProUGUI>(transform, TXT_MISS);
        txtGood = UIHelper.FindObject<TextMeshProUGUI>(transform, TXT_GOOD);
        txtLevel = UIHelper.FindObject<TextMeshProUGUI>(transform, TXT_LEVEL);

        rectMiss = txtMiss.GetComponent<RectTransform>();
        rectGood = txtGood.GetComponent<RectTransform>();

        rectComboPanel.localScale = Vector3.zero;
        defaultMissPos = rectMiss.anchoredPosition.y;
        defaultGoodPos = rectGood.anchoredPosition.y;

        txtMiss.DOFade(0f, 0f);
        txtGood.DOFade(0f, 0f);
        txtLevel.DOFade(0f, 0f);
    }

    public void ShowMiss()
    {
        txtMiss.DOFade(1f, 0.2f).
            OnComplete(()=> { txtMiss.DOFade(0f, 0.2f); });
        rectMiss.DOLocalMoveY(defaultMissPos + 100, 0.2f).
            OnComplete(() => { rectMiss.DOLocalMoveY(defaultMissPos, 0f)
                .SetDelay(0.2f); });
    }

    public void ShowGood(string status)
    {
        txtGood.DOFade(1f, 0.2f).
            OnComplete(() => { txtGood.DOFade(0f, 0.2f); });
        rectGood.DOLocalMoveY(defaultGoodPos + 100, 0.2f).
            OnComplete(() => { rectGood.DOLocalMoveY(defaultGoodPos, 0f)
                .SetDelay(0.2f); });
        txtGood.text = status;
    }

    public void ShowLevel(int level)
    {
        txtLevel.DOFade(1f, 0.2f).
            OnComplete(() => { txtLevel.DOFade(0f, 0.2f).SetDelay(1f); });
        txtLevel.text = string.Format("LEVEL {0}", level);
    }

    public void ShowCombo(bool show, int combo)
    {
        if (show)
        {
            rectComboPanel.localScale = Vector3.one * 0.75f;

            rectComboPanel.DOScale(Vector3.one, 0.3f);
            txtCombo.text = string.Format("{0}", combo);
        }
        else
        {
            rectComboPanel.DOScale(Vector3.zero, 0.3f);
        }
    }
}