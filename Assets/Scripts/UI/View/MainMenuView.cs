﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class MainMenuView : BasicView 
{
    const string BUTTON_PLAY = "Play";
    const string BUTTON_SOUND = "Sound";


    [HideInInspector]
    public Button btnPlay;

    [HideInInspector]
    public Button btnSound;

    private Text txtSound;

    public void Init()
    {
        btnPlay = UIHelper.FindObject<Button>(transform, BUTTON_PLAY);
        btnSound = UIHelper.FindObject<Button>(transform, BUTTON_SOUND);

        txtSound = btnSound.GetComponentInChildren<Text>();
    }

    public void SetButtonSoundText(bool isOn)
    {
        txtSound.text = isOn ? "SOUND ON" : "SOUND OFF";
    }
}
