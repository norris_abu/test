﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InGameController : BasicController
{
    public Halo yellowHalo;
    public Halo redHalo;

    private InGameView inGameView;
    private InputController inputController;

    public List<Note> yellowNotes = new List<Note>();
    public List<Note> redNotes = new List<Note>();

	void Awake() 
	{
		UImanager = UIManager.Instance;
		inGameView = GetComponent< InGameView > ();
        inGameView.Init();
	}

	public override void OnEnter()
	{
        base.OnEnter();

        Initialize();
        AddListener ();
    }

    public override void OnExit()
	{
        inGameView.Hide(ShowNextMenu);

        RemoveListener ();
        Reset();
        gameManager.timeController.Reset();
    }

	public override void Initialize()
	{
        inGameView.Show ();
        inputController = gameManager.inputController;

        ShowLevel(gameManager.playerController.Level);

        if (!AudioManager.Instance.isBGMOn())
            AudioManager.Instance.toggleBGM();

        if (!AudioManager.Instance.isSFXOn())
            AudioManager.Instance.toggleSFX();

        AudioManager.Instance.play_bgm(GameConstants.BGM_FROZY);
    }

	public void ShowNextMenu()
	{
		UImanager.Show (UItype);
	}

    public void ShowCombo(int combo, bool show)
    {
        inGameView.ShowCombo(show, combo);
    }

    void AddListener()
	{
        inputController.OnTapScreenBegin += OnTapBegin;
        inputController.OnTapScreenEnd += OnTapEnd;
    }

	void RemoveListener()
	{
        inputController.OnTapScreenBegin -= OnTapBegin;
        inputController.OnTapScreenEnd -= OnTapEnd;
    }

    void OnTapBegin(bool right)
    {
        if (right)
            redHalo.OnTapBegin();
        else
            yellowHalo.OnTapBegin();
    }

    void OnTapEnd(bool right)
    {
        if (right)
            redHalo.OnTapEnd();
        else
            yellowHalo.OnTapEnd();
    }

    public void Reset()
    {
        yellowNotes.Clear();
        redNotes.Clear();
    }

    public void AddNotes(Note note)
    {
        if (note.type == NoteType.RED)
            redNotes.Add(note);
        else
            yellowNotes.Add(note);
    }

    public void RemoveNotes(Note note)
    {
        if (note.type == NoteType.RED)
        {
            if(redNotes.Contains(note))
            {
                int index = redNotes.IndexOf(note);
                redNotes.RemoveAt(index);
            }
        }
        else
        {
            if (yellowNotes.Contains(note))
            {
                int index = yellowNotes.IndexOf(note);
                yellowNotes.RemoveAt(index);
            }
        }
    }

    public void ShowMiss()
    {
        inGameView.ShowMiss();
    }

    public void ShowGood(string status)
    {
        inGameView.ShowGood(status);
    }

    public void ShowLevel(int level)
    {
        inGameView.ShowLevel(level);
    }

    public void ShowGameOver()
    {
        OnClickDefault(GameState.GameOver, 0.2f);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G) && 
            gameManager.currentState == GameState.InGame)
            OnClickDefault(GameState.GameOver, 0.2f);
    }
}
