﻿using UnityEngine;
using System.Collections;

public class BasicController : MonoBehaviour 
{
	public GameState controllerType;
	protected UIManager UImanager;
    public GameState UItype;

	protected GameManager gameManager;

	void Start()
	{
        if (gameManager == null)
            gameManager = GameManager.Instance;
	}

	public virtual void Initialize()
	{
		UImanager = UIManager.Instance;
	}

	public virtual void OnEnter()
	{
        if (gameManager == null)
            gameManager = GameManager.Instance;
        
        gameManager.SetGameState(controllerType);
	}


    public virtual void OnExit()
	{
		
	}

	protected void OnClickDefault(GameState type, float time = 1)
	{
		AudioManager.Instance.play_sfx ("Audio/Click", Vector3.zero);
        UItype = type;
        UImanager.Hide(controllerType);
	}

    public void ShowNextState(GameState type)
    {
        UItype = type;
        UImanager.Hide(controllerType);
        UImanager.Show(type);
    }
}
