﻿using UnityEngine;
using System.Collections;

public class MainMenuController : BasicController 
{
	private MainMenuView mainMenuView;
	void Awake() 
	{
		UImanager = UIManager.Instance;
		mainMenuView = GetComponent< MainMenuView > ();

        mainMenuView.Init();
	}

	public override void OnEnter()
	{
        base.OnEnter();
        mainMenuView.Show();

		AddListener ();
		Initialize ();
	}

    public override void OnExit()
	{
		RemoveListener ();
        mainMenuView.Hide (ShowNextMenu);
	}

	public override void Initialize()
	{
        bool isOn = AudioManager.Instance.isBGMOn();
        mainMenuView.SetButtonSoundText(isOn);

        AudioManager.Instance.play_bgm(GameConstants.BGM_SYSTEMSHOCK);
	}

	public void ShowNextMenu()
	{
		UImanager.Show (UItype);
	}

	void AddListener()
	{
        mainMenuView.btnPlay.onClick.AddListener(OnClickPlay);
        mainMenuView.btnSound.onClick.AddListener(OnClickSound);
    }

	void RemoveListener()
	{
        mainMenuView.btnPlay.onClick.RemoveListener(OnClickPlay);
        mainMenuView.btnSound.onClick.RemoveListener(OnClickSound);
    }

	void OnClickPlay()
	{
		OnClickDefault (GameState.InGame, 0.2f);

        AudioManager.Instance.play_sfx(GameConstants.SFX_MENU_SELECT,
           transform.position);
    }

    void OnClickSound()
    {
        AudioManager.Instance.toggleBGM();
        AudioManager.Instance.toggleSFX();

        AudioManager.Instance.play_sfx(GameConstants.SFX_MENU_SELECT,
           transform.position);

        bool isOn = AudioManager.Instance.isBGMOn();
        mainMenuView.SetButtonSoundText(isOn);
    }
}
