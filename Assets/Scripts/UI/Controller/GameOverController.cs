﻿using UnityEngine;
using System.Collections;

public class GameOverController : BasicController
{
	private GameOverView gameOverView;

	void Awake() 
	{
		UImanager = UIManager.Instance;
		gameOverView = GetComponent< GameOverView > ();

        gameOverView.Init();
	}

	public override void OnEnter()
	{
        base.OnEnter();

		AddListener ();
		Initialize ();
	}

    public override void OnExit()
	{
		RemoveListener ();
        gameOverView.Hide (ShowNextMenu);
	}

	public override void Initialize()
	{
        gameOverView.Show ();

        bool isOn = AudioManager.Instance.isBGMOn();
        gameOverView.SetButtonSoundText(isOn);

        gameOverView.SetScore(gameManager.playerController.Score);
        gameManager.playerController.Reset();

        AudioManager.Instance.play_bgm(GameConstants.BGM_TWILIGHT);
    }

	public void ShowNextMenu()
	{
		UImanager.Show (UItype);
	}

	void AddListener()
	{
        gameOverView.btnPlay.onClick.AddListener(OnClickPlay);
        gameOverView.btnHome.onClick.AddListener(OnClickHome);
        gameOverView.btnShare.onClick.AddListener(OnClickShare);
        gameOverView.btnSound.onClick.AddListener(OnClickSound);
	}

	void RemoveListener()
	{
        gameOverView.btnPlay.onClick.RemoveListener(OnClickPlay);
        gameOverView.btnHome.onClick.RemoveListener(OnClickHome);
        gameOverView.btnShare.onClick.RemoveListener(OnClickShare);
        gameOverView.btnSound.onClick.RemoveListener(OnClickSound);
	}

    void OnClickPlay()
    {
        OnClickDefault(GameState.InGame, 0.2f);
    }

    void OnClickHome()
    {
        OnClickDefault(GameState.MainMenu, 0.2f);
    }

	void OnClickShare()
	{

	}

	void OnClickSound()
	{
        AudioManager.Instance.toggleBGM();
        AudioManager.Instance.toggleSFX();

        AudioManager.Instance.play_sfx(GameConstants.SFX_MENU_SELECT,
           transform.position);

        bool isOn = AudioManager.Instance.isBGMOn();
        gameOverView.SetButtonSoundText(isOn);
    }
}
