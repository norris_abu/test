﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Move : MonoBehaviour
{
    public float speed = 5;
    public float defaultSpeed = 5;

    private Transform objectTransform;
    Vector3 bottomScreenPosition;
    float bottomOffset = 1f;

    public UnityAction OnOutOfScreen;

    Vector3 yellowHaloPosition;
    Vector3 redHaloPosition;

    Note note;
    // Start is called before the first frame update
    void Start()
    {
        note = GetComponent<Note>();
        objectTransform = transform;

        bottomScreenPosition = Camera.main.ScreenToWorldPoint(Vector3.zero);
        bottomScreenPosition.y -= bottomOffset;

        yellowHaloPosition = GameManager.Instance.inGameController.yellowHalo.transform.position;
        redHaloPosition = GameManager.Instance.inGameController.redHalo.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameObject.activeSelf)
            return;

        Vector3 position = objectTransform.position;
        position.y -= Time.deltaTime * (GameManager.Instance.timeController.SpeedTempo * speed);
        objectTransform.position = position;

        if(note != null)
        {
            if (note.type == NoteType.RED)
                IsOfScreen(objectTransform.position, redHaloPosition);
            else
                IsOfScreen(objectTransform.position, yellowHaloPosition);
        }

        if (position.y < bottomScreenPosition.y)
        {
            gameObject.SetActive(false);

            if (OnOutOfScreen != null)
                OnOutOfScreen();
        }
    }

    void IsOfScreen(Vector3 note, Vector3 halo)
    {
        float distance = Vector3.Distance(note, halo);
        if (distance > 1.2f && note.y < halo.y)
        {
            gameObject.SetActive(false);

            if (OnOutOfScreen != null)
                OnOutOfScreen();
        }
    }
}
