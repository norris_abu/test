﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    public NoteType type;

    Move move;
    InGameController ingameController;
    // Start is called before the first frame update
    void Start()
    {
        ingameController = GameManager.Instance.inGameController;
        move = GetComponent<Move>();

        move.OnOutOfScreen += OutOfScreen;
    }

    void OutOfScreen()
    {
        ingameController.RemoveNotes(this);
        ingameController.ShowCombo(0, false);
        ingameController.ShowMiss();

        GameManager.Instance.playerController.ResetCombo();
        GameManager.Instance.playerController.AddMiss();

        transform.position = new Vector3(0, 10, 0f);
    }
}

public enum NoteType
{
    RED,
    YELLOW
}