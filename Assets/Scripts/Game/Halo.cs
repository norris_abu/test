﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Halo : MonoBehaviour
{
    public NoteType type;
    public SpriteRenderer glowEffect;

    InGameController ingameController;
    PlayerController playerController;

    int combo = 0;
    private void Start()
    {
        ingameController = GameManager.Instance.inGameController;
        playerController = GameManager.Instance.playerController;
    }

    public void OnTapBegin()
    {
        CheckTapNote(type == NoteType.RED ? 
            ingameController.redNotes : 
            ingameController.yellowNotes);

        glowEffect.DOFade(1, 0.2f);
    }

    void CheckTapNote(List<Note> notes)
    {
        if (notes.Count > 0)
        {
            Note note = notes[0];

            float distance = Vector3.Distance(note.transform.position,
                transform.position);

            float notePos = note.transform.position.y;
            float haloPos = transform.position.y;
            string status = "GOOD";

            if (distance <= 1)
            {
                playerController.AddCombo();
                playerController.AddScore();

                if (notePos >= haloPos)
                {
                    if (distance <= 0.5f)
                        status = "EXCELLENT";
                }

                note.gameObject.SetActive(false);
                ingameController.RemoveNotes(note);
                ingameController.ShowCombo(playerController.Combo, true);
                ingameController.ShowGood(status);

                AudioManager.Instance.play_sfx(GameConstants.SFX_HIT_YELLOWNOTE,
                    transform.position);
            }
            else
            {
                if (notePos < haloPos)
                {
                    note.gameObject.SetActive(false);
                    ingameController.RemoveNotes(note);
                }
                AudioManager.Instance.play_sfx(GameConstants.SFX_TAPHALO_DEFAULT,
                  transform.position);
            }
        }
    }

    public void OnTapEnd()
    {
        glowEffect.DOFade(0.2f, 0.2f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.LogError(collision.gameObject.name);
    }
}
