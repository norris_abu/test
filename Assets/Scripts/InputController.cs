﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputController : MonoBehaviour
{
    public UnityAction<bool> OnTapScreenBegin;
    public UnityAction<bool> OnTapScreen;
    public UnityAction<bool> OnTapScreenEnd;

    Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR

        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (OnTapScreenBegin != null)
                OnTapScreenBegin(false);
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (OnTapScreenEnd != null)
                OnTapScreenEnd(false);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (OnTapScreenBegin != null)
                OnTapScreenBegin(true);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (OnTapScreenEnd != null)
                OnTapScreenEnd(true);
        }

#else
        int touchCount = Input.touchCount;
        for(int i = 0; i < touchCount; i++)
        {
            if (i == 2)
                return;

            Touch touch = Input.GetTouch(i);
            bool isRight = IsRight(touch.position);
            if (touch.phase == TouchPhase.Began)
            {
                if (OnTapScreenBegin != null)
                    OnTapScreenBegin(isRight);
            }

            if(touch.phase == TouchPhase.Moved || 
                touch.phase == TouchPhase.Stationary)
            {
                if (OnTapScreen != null)
                    OnTapScreen(isRight);
            }

            if(touch.phase == TouchPhase.Ended)
            {
                if (OnTapScreenEnd != null)
                    OnTapScreenEnd(isRight);
            }
        }
#endif
    }

    bool IsRight(Vector3 position)
    {
        bool isRight = true;
        if (position.x < Screen.width / 2)
            isRight = false;
        return isRight;
    }
}
