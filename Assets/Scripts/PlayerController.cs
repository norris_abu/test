﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    const int MAX_MISS = 5;

    private int combo = 0;
    public int Combo { get { return combo; } }

    private int score = 0;
    public int Score { get { return score; } }

    private int level = 1;
    public int Level { get { return level; } }

    private int miss = 0;

    public void AddCombo()
    {
        combo++;
    }

    public void ResetCombo()
    {
        combo = 0;
    }

    public void AddMiss()
    {
        miss++;
        if(miss > MAX_MISS)
        {
            GameManager.Instance.inGameController.ShowGameOver();
        }
    }

    public void ResetMiss()
    {
        miss = 0;
    }

    public void AddScore()
    {
        score++;
    }

    public void ResetScore()
    {
        score = 0;
    }

    public void AddLevel()
    {
        level++;
    }

    public void ResetLevel()
    {
        level = 1;
    }

    public void Reset()
    {
        ResetCombo();
        ResetMiss();
        ResetScore();
        ResetLevel();
    }
}
